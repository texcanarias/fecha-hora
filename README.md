# Fecha-hora
Proyecto para poner en conjunto diferentes técnicas como traits, interfaces, pruebas unitarias.

#Pasos

##Instalación de composer
Se debe tener instalado composer.

composer install

Con ello se instalan las dependencias phpunit.

##Ejecución de pruebas
Para ejecutar las pruebas:

./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/<Nombre del test>


