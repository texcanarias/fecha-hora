<?php
namespace Src\Common\Time;

trait Ajustar_cadena_trait{
    
    public function AjustarCadena(int $Item):string {
        $Priv = (int) $Item;
        $Cero = ($Priv < 10) ? "0" : "";
        return $Cero . $Priv;
    }    
}