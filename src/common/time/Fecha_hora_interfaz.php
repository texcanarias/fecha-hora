<?php

namespace Src\Common\Time;

use Src\Common\Time\Fecha_interfaz;
use Src\Common\Time\Hora_interfaz;

/**
 * Clase de Fecha y Hora
 *
 * @author antonio
 */
interface Fecha_hora_interfaz  {

    public function getHora();

    public function setHora(Hora_interfaz $Hora);

    public function getFecha();

    public function setFecha(Fecha_interfaz $Fecha);

    public function mayorQue(Fecha_hora_interfaz $FechaHora);

    public function menorQue(Fecha_hora_interfaz $FechaHora);

    public function igualQue(Fecha_hora_interfaz $FechaHora);    
    
    
}
