<?php

namespace Src\Common\Time;

use Src\Common\Time\Hora_model;
use Src\Common\Time\Fecha_model;

/**
 * Clase de Fecha y Hora
 *
 * @author antonio
 */
class Fecha_hora_model {

    private $Hora;
    private $Fecha;


    function __construct() {
        $this->Hora = new Hora_model();
        $this->Fecha = new Fecha_model();
    }

    public function getHora() {
        return $this->Hora;
    }

    public function setHora(Hora_model $Hora) {
        $this->Hora = $Hora;
    }

    public function getFecha() {
        return $this->Fecha;
    }

    public function setFecha(Fecha_model $Fecha) {
        $this->Fecha = $Fecha;
    }

    /**
     * Señala si la fecha del objeto es mayor que otra que se pasa por parámetro
     *
     * @param Fecha_model $FechaHora
     */
    public function mayorQue(Fecha_hora_interfaz $FechaHora):bool {
        if($this->getFecha() > $FechaHora->getFecha()){
            return true;
        }
        else{
            if ($this->getFecha() == $FechaHora->getFecha()){
                if($this->getHora() > $FechaHora->getHora()){
                    return true;
                }
            }
        }
        return false;
    }

    public function menorQue(Fecha_hora_interfaz $FechaHora):bool {
        if($this->getFecha() < $FechaHora->getFecha()){
            return true;
        }
        else{
            if ($this->getFecha() == $FechaHora->getFecha()){
                if($this->getHora() < $FechaHora->getHora()){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Señala si la fecha del objeto es igual que otra que se pasa por parámetro
     *
     * @param Fecha_model $Fecha
     */
    public function igualQue(Fecha_hora_interfaz $FechaHora):bool {
        return ($this->getFecha() == $FechaHora->getFecha() && $this->getHora() == $FechaHora->getHora());
    }


    public static function factoriaFechaHoraModel(Fecha_interfaz $Fecha, Hora_interfaz $Hora):Fecha_hora_model {
        $Item = new Fecha_hora_model();
        $Item->setFecha($Fecha);
        $Item->setHora($Hora);
        return $Item;
    }

    public function __toString():string {
        return $this->Fecha . " " . $this->Hora;
    }

}
