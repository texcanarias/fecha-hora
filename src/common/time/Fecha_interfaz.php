<?php

namespace Src\Common\Time;

/**
 * interface fecha
 */
interface Fecha_interfaz {

    public function getFecha();

    public function setFecha(string $Fecha);

    public function getDia();

    public function getMes();

    public function getAnyo();

    public function setDia(int $Dia);

    public function setMes(int $Mes);

    public function setAnyo(int $Anyo);

    /**
     * Señala si la fecha del objeto es mayor que otra que se pasa por 
     * parámetro
     *
     * @param Fecha_interfaz $Fecha
     */
    public function mayorQue(Fecha_interfaz $Fecha):bool;

    public function menorQue(Fecha_interfaz $Fecha):bool;

    /**
     * Señala si la fecha del objeto es igual que otra que se pasa por parámetro
     *
     * @param Fecha_interfaz $Fecha
     */
    public function igualQue(Fecha_interfaz $Fecha):bool;

    public function getFechaLinux();

    public static function factoriaFechaModel(string $Fecha = ""):Fecha_interfaz;

    public static function factoriaFechaModelPorElementos(int $Anyo, int $Mes, int $Dia);

    public function __toString():string;

    public function isFinDeSemana():bool;

    public function truncarInicioMes():Fecha_interfaz;

    public function truncarFinMes():Fecha_interfaz;

    /**
     * Método que suma días a la fecha actual.
     * @param int $Dias Número de dias a incrementar, por defecto 1
     */
    public function sumarDias(int $Dias = 1):Fecha_interfaz;

    public function desplazarPrincipiosSemana():Fecha_interfaz;

    public function desplazarFinalesSemana();

    public function getNumeroSemana();

    public function getNumeroUltimaSemanaAnyo():int;

    public function getCardinalSemanaDomingoMarca7():int;

    public function isLunes():bool;

    public function isDomingo():bool;

    public function isFinSemana():bool;

    public function getFechaLinuxParametrizado(string $Fecha);
}
