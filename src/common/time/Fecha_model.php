<?php
namespace Src\Common\Time;

use Src\Common\Time\Fecha_interfaz;
use Src\Common\Time\Ajustar_cadena_trait;

/**
 * Clase fecha
 */
class Fecha_model implements Fecha_interfaz {

    use Ajustar_cadena_trait;
    
    /**
     * @var string Fecha en formato Y-m-d
     */
    protected $Fecha;
    protected $Dia;
    protected $Mes;
    protected $Anyo;
    protected static $PosicionAnyo = 0;
    protected static $PosicionMes = 1;
    protected static $PosicionDia = 2;

    public function __construct() {
        $this->Fecha = date("Y-m-d");
        $this->descomponer();
    }

    public function getFecha():string {
        return $this->Fecha;
    }

    public function setFecha(string $Fecha) {
        $this->Fecha = $Fecha;
        $this->descomponer();
        return $this;
    }

    protected function componer() {
        $this->Fecha = $this->Anyo . "-" . $this->Mes . "-" . $this->Dia;
    }

    protected function descomponer() {
        $Fecha = explode(" ", $this->Fecha);
        $Atoms = explode("-", $Fecha[0]);
        if (3 != count($Atoms)) {
            $this->Fecha = date("Y-m-d");
            $this->descomponer();
        } else {
            $this->Dia = $Atoms[self::$PosicionDia];
            $this->Mes = $Atoms[self::$PosicionMes];
            $this->Anyo = $Atoms[self::$PosicionAnyo];
        }
    }
    
    public function getDia():string {
        return $this->AjustarCadena($this->Dia);
    }

    public function getMes():string {
        return $this->AjustarCadena($this->Mes);
    }

    public function getAnyo():string {
        return $this->AjustarCadena($this->Anyo);
    }

    public function setDia(int $Dia):Fecha_model {
        $this->Dia = $Dia;
        $this->componer();
        return $this;
    }

    public function setMes(int $Mes):Fecha_model {
        $this->Mes = $Mes;
        $this->componer();
        return $this;
    }

    public function setAnyo(int $Anyo):Fecha_model {
        $this->Anyo = $Anyo;
        $this->componer();
        return $this;
    }

    public function mayorQue(Fecha_interfaz $Fecha):bool {
        $TiempoLinuxObjeto = $this->getFechaLinuxParametrizado($this->Fecha);
        $TiempoLinuxForanea = $this->getFechaLinuxParametrizado($Fecha);
        return ($TiempoLinuxObjeto > $TiempoLinuxForanea);
    }

    public function menorQue(Fecha_interfaz $Fecha):bool {
        $TiempoLinuxObjeto = $this->getFechaLinuxParametrizado($this->Fecha);
        $TiempoLinuxForanea = $this->getFechaLinuxParametrizado($Fecha);
        return ($TiempoLinuxObjeto < $TiempoLinuxForanea);
    }

    public function igualQue(Fecha_interfaz $Fecha):bool {
        return ($this->Fecha == $Fecha->getFecha());
    }

    public function getFechaLinux() {
        return mktime(0, 0, 0, $this->getMes(), $this->getDia(), $this->getAnyo());
    }

    public function getFechaLinuxParametrizado(string $Fecha) {
        $VFecha = explode("-", $Fecha);
        return mktime(0, 0, 0, $VFecha[self::$PosicionMes], $VFecha[self::$PosicionDia], $VFecha[self::$PosicionAnyo]);
    }

    public static function factoriaFechaModel(string $Fecha = ""):Fecha_interfaz {
        $Item = new Fecha_model();
        if ("" != $Fecha) {
            $Fecha = substr($Fecha, 0, 10);            
            $Item->setFecha($Fecha);
        }
        return $Item;
    }

    public static function factoriaFechaModelPorElementos(int $Anyo, int $Mes, int $Dia) {
        $Item = new Fecha_model();
        $Item->setFecha($Anyo . "-" . $Mes . "-" . $Dia);
        return $Item;
    }

    public function __toString():string {
        return $this->Fecha;
    }

    public function isFinDeSemana():bool {
        $NumeroDiaSemana = date("w", $this->getFechaLinux());
        $isSabado = (6 == $NumeroDiaSemana);
        $isDomingo = (0 == $NumeroDiaSemana);
        $isFinDeSemana = ($isSabado || $isDomingo);
        return $isFinDeSemana;
    }

    public function truncarInicioMes():Fecha_interfaz {
        $this->Dia = 1;
        $this->componer();
        return $this;
    }

    public function truncarFinMes():Fecha_interfaz {
        $this->Dia = date("t", $this->getFechaLinux());
        $this->componer();
        return $this;
    }

    /**
     * Método que suma días a la fecha actual.
     * @param int $Dias Número de dias a incrementar, por defecto 1
     */
    public function sumarDias(int $Dias = 1):Fecha_interfaz {
        $this->Fecha = date("Y-m-d", mktime(0, 0, 0, $this->Mes, $this->Dia + $Dias, $this->Anyo));
        $this->descomponer();
        return $this;
    }

    public function desplazarPrincipiosSemana():Fecha_interfaz  {
        $Actual = date("N", $this->getFechaLinux());
        $Objetivo = $Actual - 1;
        return $this->sumarDias(-$Objetivo);
    }

    public function desplazarFinalesSemana():Fecha_interfaz  {
        $Actual = date("N", $this->getFechaLinux());
        $Objetivo = 7 - $Actual;
        return $this->sumarDias($Objetivo);
    }

    public function getNumeroSemana():int {
        $NumeroSemana = date("W", $this->getFechaLinux());
        return $NumeroSemana;
    }

    public function getNumeroUltimaSemanaAnyo():int {
        $NumeroSemana = date("W", mktime(0, 0, 0, 12, 31, $this->Anyo));
        return $NumeroSemana;
    }

    public function getCardinalSemanaDomingoMarca7():int {
        $NumeroDiaSemana = date("N", $this->getFechaLinux());
        return $NumeroDiaSemana;
    }

    public function isLunes():bool {
        return (1 == $this->getCardinalSemanaDomingoMarca7());
    }

    public function isDomingo():bool {
        return (7 == $this->getCardinalSemanaDomingoMarca7());
    }

    public function isFinSemana():bool {
        return (6 == $this->getCardinalSemanaDomingoMarca7() || 7 == $this->getCardinalSemanaDomingoMarca7());
    }
}
