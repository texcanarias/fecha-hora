<?php

namespace Src\Common\Time;

/**
 * Interfaz de hora (tiempo)
 *
 * @author antonio
 */
interface Hora_interfaz {

    public function getHora();

    public function setHora(string $Hora);

    public function getHoraDetalle();

    public function getMinuto();

    public function getSegundo();

    public function setHoraDetalle(int $HoraDetalle);

    public function setMinuto(int $Minuto);

    public function setSegundo(int $Segundo);

    public static function factoriaHoraModel(int $Hora);

    public function getTiempoEnSegundos();

}
