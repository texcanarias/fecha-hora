<?php
namespace Src\Common\Time;

use Src\Common\Time\Hora_interfaz;
use Src\Common\Time\Ajustar_cadena_trait;

/**
 * Clase de Fecha y Hora
 *
 * @author antonio
 */
class Hora_model implements Hora_interfaz {

    use Ajustar_cadena_trait;
    
    private static $PosicionHora = 0;
    private static $PosicionMinuto = 1;
    private static $PosicionSegundo = 2;

    /**
     * @var string Hora en formato hh:mm:ss
     */
    protected $Hora;
    protected $HoraDetalle;
    protected $Minuto;
    protected $Segundo;

    function __construct() {
        $this->Hora = date("H:i:s");
        $this->descomponer();
    }

    public function getHora() {
        return $this->Hora;
    }

    public function setHora(string $Hora) {
        $this->Hora = $this->ajustarHoraSiNoExistenSegundos($Hora);
        $this->descomponer();
    }

    protected function componer() {
        $this->Hora = $this->AjustarCadena($this->HoraDetalle) . ":" . $this->AjustarCadena($this->Minuto) . ":" . $this->AjustarCadena($this->Segundo);
    }

    protected function descomponer() {
        $Atoms = explode(":", $this->Hora);
        
        $this->HoraDetalle = (1 <= count($Atoms))?$Atoms[self::$PosicionHora]:"00";
        $this->Minuto = (2 <= count($Atoms))?$Atoms[self::$PosicionMinuto]:"00";
        $this->Segundo = (3 <= count($Atoms))?$Atoms[self::$PosicionSegundo]:"00";
    }

    public function getHoraDetalle() {
        return $this->AjustarCadena($this->HoraDetalle);
    }

    public function getMinuto() {
        return $this->AjustarCadena($this->Minuto);
    }

    public function getSegundo() {
        return $this->AjustarCadena($this->Segundo);
    }

    public function setHoraDetalle(int $HoraDetalle) {
        $this->HoraDetalle = $HoraDetalle;
        $this->componer();
        return $this;
    }

    public function setMinuto(int $Minuto) {
        $this->Minuto = $Minuto;
        $this->componer();
        return $this;
    }

    public function setSegundo(int $Segundo) {
        $this->Segundo = $Segundo;
        $this->componer();
        return $this;
    }

    public function mayorQue(Hora_interfaz $Hora):bool {
        return ($this->getHora() > $Hora->getHora());
    }

    public function menorQue(Hora_interfaz $Hora):bool {
        return ($this->getHora() < $Hora->getHora());
    }

 
    public function igualQue(Hora_interfaz $Hora): bool {
        return ($this->getHora() == $Hora->getHora());
    }

    public static function factoriaHoraModel(int $Hora = 0) {
        $Item = new Hora_model();
        $Item->setHora($Hora);
        return $Item;
    }

    public function __toString() {
        return $this->Hora;
    }

    public function getTiempoEnSegundos() {
        return ($this->HoraDetalle * 3600) + ($this->Minuto * 60) + $this->Segundo;
    }

    private function ajustarHoraSiNoExistenSegundos(string $Horas):string {
        $LongitudCadenaSiSoloHayHorasYMinutos = 5;
        $Horas .= ($LongitudCadenaSiSoloHayHorasYMinutos == strlen($Horas)) ? ":00" : "";
        return $Horas;
    }
}
