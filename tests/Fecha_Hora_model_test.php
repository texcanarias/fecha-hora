<?php
//./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/*

declare(strict_types=1);

use Src\Common\Time\Fecha_hora_model;
use Src\Common\Time\Fecha_model;
use Src\Common\Time\Hora_model;
use PHPUnit\Framework\TestCase;

final class Fecha_Hora_model_test extends TestCase{
       
    public function testGetFechaHora(): void{
        $item = new Fecha_hora_model();
        $item->setFecha(Fecha_model::factoriaFechaModelPorElementos(2018, 6, 26));
        $item->setHora(Hora_model::factoriaHoraModel(12));

        $this->assertSame("2018", $item->getFecha()->getAnyo());
        $this->assertSame("06", $item->getFecha()->getMes());        
        $this->assertSame("26", $item->getFecha()->getDia());          
        
        $this->assertSame("12", $item->getHora()->getHoraDetalle());
        $this->assertSame("00", $item->getHora()->getMinuto());        
        $this->assertSame("00", $item->getHora()->getSegundo());        
    } 
}