<?php
//./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/*

declare(strict_types=1);

use Src\Common\Time\Fecha_model;
use PHPUnit\Framework\TestCase;

final class Fecha_model_test extends TestCase{
       
    public function testGetFecha(): void{
        $fecha = new Fecha_model();
        $fecha->setFecha("2018-06-26");
        
        $this->assertSame("2018", $fecha->getAnyo());
        $this->assertSame("06", $fecha->getMes());        
        $this->assertSame("26", $fecha->getDia());        
    }

    public function testGetSoloAnyo(): void{
        $fecha = new Fecha_model();
        $fecha->setFecha("2018-06-26");
        $fecha->setAnyo(2019);
        
        $this->assertSame("2019", $fecha->getAnyo());
        $this->assertSame("06", $fecha->getMes());        
        $this->assertSame("26", $fecha->getDia());        
    }
    
    public function testGetSoloMes(): void{
        $fecha = new Fecha_model();
        $fecha->setFecha("2018-06-26");
        $fecha->setMes(7);
        
        $this->assertSame("2018", $fecha->getAnyo());
        $this->assertSame("07", $fecha->getMes());        
        $this->assertSame("26", $fecha->getDia());        
    }
    
    public function testGetSoloDia(): void{
        $fecha = new Fecha_model();
        $fecha->setFecha("2018-06-26");
        $fecha->setDia(27);
        
        $this->assertSame("2018", $fecha->getAnyo());
        $this->assertSame("06", $fecha->getMes());        
        $this->assertSame("27", $fecha->getDia());        
    }
    

    public function testMayorQue(): void{
        $fecha = new Fecha_model();
        $fecha->setFecha("2018-06-26");
        $fechaComparada = clone($fecha);

        $resIgual = $fecha->mayorQue($fechaComparada);
        $this->assertSame(false, $resIgual);
        
        $fechaComparada->setFecha("2018-06-27");
        $resMenor = $fecha->mayorQue($fechaComparada);
        $this->assertSame(false, $resMenor);
        
        $fechaComparada->setFecha("2018-06-25");
        $resMayor = $fecha->mayorQue($fechaComparada);
        $this->assertSame(true, $resMayor);
        
    }

    public function testMenorQue(): void{
        $fecha = new Fecha_model();
        $fecha->setFecha("2018-06-26");
        $fechaComparada = clone($fecha);

        $resIgual = $fecha->menorQue($fechaComparada);
        $this->assertSame(false, $resIgual);
        
        $fechaComparada->setFecha("2018-06-27");
        $resMenor = $fecha->menorQue($fechaComparada);
        $this->assertSame(true, $resMenor);
        
        $fechaComparada->setFecha("2018-06-25");
        $resMayor = $fecha->menorQue($fechaComparada);
        $this->assertSame(false, $resMayor);
        
    }

    public function testIgualQue(): void{
        $fecha = new Fecha_model();
        $fecha->setFecha("2018-06-26");
        $fechaComparada = clone($fecha);

        $resIgual = $fecha->igualQue($fechaComparada);
        $this->assertSame(true, $resIgual);
        
        $fechaComparada->setFecha("2018-06-27");
        $resMenor = $fecha->igualQue($fechaComparada);
        $this->assertSame(false, $resMenor);
        
        $fechaComparada->setFecha("2018-06-25");
        $resMayor = $fecha->igualQue($fechaComparada);
        $this->assertSame(false, $resMayor);        
    }
    
    public function testIsFinSemana():void{
        $fecha = new Fecha_model();
        $fecha->setFecha("2018-06-26");
        $isFinSemana = $fecha->isFinDeSemana();
        $this->assertSame(false, $isFinSemana);
        
        $fecha->setFecha("2018-06-24");
        $isFinSemana = $fecha->isFinDeSemana();
        $this->assertSame(true, $isFinSemana);        
    }
    
    
 
}