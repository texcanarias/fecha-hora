<?php
//./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/*

declare(strict_types=1);

use Src\Common\Time\Hora_model;
use PHPUnit\Framework\TestCase;

final class Hora_model_test extends TestCase{
       
    public function testGetHora(): void{
        $hora = new Hora_model();
        $hora->setHora("12:20:40");
        
        $this->assertSame("12", $hora->getHoraDetalle());
        $this->assertSame("20", $hora->getMinuto());        
        $this->assertSame("40", $hora->getSegundo());        
    }

    public function testGetSoloHora(): void{
        $hora = new Hora_model();
        $hora->setHora("12");

        $this->assertSame("12", $hora->getHoraDetalle());
        $this->assertSame("00", $hora->getMinuto());        
        $this->assertSame("00", $hora->getSegundo());        

    }
    
    public function testGetSoloHoraMinuto(): void{
        $hora = new Hora_model();
        $hora->setHora("12:20");

        $this->assertSame("12", $hora->getHoraDetalle());
        $this->assertSame("20", $hora->getMinuto());        
        $this->assertSame("00", $hora->getSegundo());        
    }

    public function testMayorQue(): void{
        $hora = new Hora_model();
        $hora->setHora("12:20:40");
        $horaComparada = clone($hora);

        $resIgual = $hora->mayorQue($horaComparada);
        $this->assertSame(false, $resIgual);
        
        $horaComparada->setHora("12:25:40");
        $resMenor = $hora->mayorQue($horaComparada);
        $this->assertSame(false, $resMenor);
        
        $horaComparada->setHora("11:25:40");
        $resMayor = $hora->mayorQue($horaComparada);
        $this->assertSame(true, $resMayor);
        
    }

    public function testMenorQue(): void{
        $hora = new Hora_model();
        $hora->setHora("12:20:40");
        $horaComparada = clone($hora);

        $resIgual = $hora->menorQue($horaComparada);
        $this->assertSame(false, $resIgual);
        
        $horaComparada->setHora("12:25:40");
        $resMenor = $hora->menorQue($horaComparada);
        $this->assertSame(true, $resMenor);
        
        $horaComparada->setHora("11:25:40");
        $resMayor = $hora->menorQue($horaComparada);
        $this->assertSame(false, $resMayor);
        
    }

    public function testIgualQue(): void{
        $hora = new Hora_model();
        $hora->setHora("12:20:40");
        $horaComparada = clone($hora);

        $resIgual = $hora->igualQue($horaComparada);
        $this->assertSame(true, $resIgual);
        
        $horaComparada->setHora("12:25:40");
        $resMenor = $hora->igualQue($horaComparada);
        $this->assertSame(false, $resMenor);
        
        $horaComparada->setHora("11:25:40");
        $resMayor = $hora->igualQue($horaComparada);
        $this->assertSame(false, $resMayor);
        
    }
 
}